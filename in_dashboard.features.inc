<?php
/**
 * @file
 * in_dashboard.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function in_dashboard_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
