<?php
/**
 * @file
 * in_dashboard.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function in_dashboard_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access dashboard'.
  $permissions['access dashboard'] = array(
    'name' => 'access dashboard',
    'roles' => array(
      'admin' => 'admin',
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'dashboard',
  );

  return $permissions;
}
